
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('public/assets/img/apple-icon.png') }}">

  <link rel="icon" type="image/png" href="{{ asset('public/assets/img/favicon.png') }}">
     
  <title>B.Y.T.E.S | Dashboard</title>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>User Data</h2>
  <p></p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Data</th>
        <th>Detail</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><strong>Name-</strong></td>
        <td>{{$name}}</td>
      </tr>
      <tr>
        <td><strong>Email-</strong></td>
        <td>{{$email}}</td>
      </tr>
      <tr>
        <td><strong>Phone-</strong></td>
        <td>{{$phone}}</td>
      </tr>
      <tr>
        <td><strong>Subject-</strong></td>
        <td>{{$subject}}</td>
      </tr>
      <tr>
        <td><strong>Message-</strong></td>
        <td>{{$sms}}</td>
      </tr>
    </tbody>
  </table>
</div>

</body>
</html>
