<?php $id = $data['id']?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

     <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('public/assets/img/apple-icon.png') }}">

     <link rel="icon" type="image/png" href="{{ asset('public/assets/img/favicon.png') }}">
     
     <title>B.Y.T.E.S | Dashboard</title>

    <style type="text/css">

        body

        {

            font-family: Arial;

            font-size: 10pt;

        }

        table

        {

            border: 1px solid #ccc;

            border-collapse: collapse;

        }

        table th

        {

            background-color: #F7F7F7;

            color: #333;

            font-weight: bold;

        }

        table th, table td

        {

            padding: 5px;

            border: 1px solid #ccc;

        }

    </style>

</head>

<body>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if (\Session::has('update'))

<script>

swal ( "Update" ,  "Password update successfully" ,  "update" );

</script>

@endif

@if (\Session::has('otherupdate'))

<script>

swal ( "NotUpdate" ,  "Use another password" ,  "otherupdate" );

</script>

@endif	

<center>

<h2>Change Password</h2>

<form action="{{url('updateadminpass')}}" method="post">

    <table border="0" cellpadding="3" cellspacing="0">

        <tr>

            <td>

                Password:

            </td>

            <td>

                <input type="password" id="txtPassword" name="password" />

            </td>

        </tr>

        <tr>

            <td>

                Confirm Password:

            </td>

            <td>

                <input type="password" id="txtConfirmPassword"/>

            </td>

        </tr>

        <tr>

        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
        	<input type="hidden" name="userid" ng-modal="userid" value={{$id}}>

        </tr>

        <tr>

            <td>

            </td>

            <td>

                <button type="submit" class="btn btn-info mr-3" onclick="return Validate()">Submit</button> 

            </td>

        </tr>	

    </table>

</form>    

</center>    

    <script type="text/javascript">

        function Validate() {

            var password = document.getElementById("txtPassword").value;

            var confirmPassword = document.getElementById("txtConfirmPassword").value;

            if (password != confirmPassword) {

                alert("Passwords do not match.");

                return false;

            }

            return true;

        }

    </script>

</body>

</html>

