@include('include.header')
<?php $subcatid = $data['subcatid'];?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if (\Session::has('success'))
<script>
swal ( "Success" ,  "Images has been uploaded" ,  "success" );
</script>
@endif
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if (\Session::has('deleted'))
<script>
swal ( "Success" ,  "Image has been deleted" ,  "success" );
</script>
@endif
      <!-- End Navbar -->  
      <div class="content page_data">
        <div class="mb-5">
          <p class="mb-0 fz35 pt-3">Images</p>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger hide">
            <strong>Sorry!</strong><br>
            <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
            </ul>
            </div>
        @endif
          <div class="container">
            <div class="row">
              <div class="col-12 col-sm-6 col-md-3">
                <div class="col_selector mb-4">
                <form action="{{ url('addimages') }}" method="post" enctype="multipart/form-data" class="img_upload_form">  
                  <div class="img_upload">
                    <label for="customFile" class="mb-0" title="Click to upload images">
                      <i class="fa fa-upload"></i>
                    </label>
                    <input type="file" multiple name="filename[]" class="d-none" id="customFile">
                  </div>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="subid" value="{{$subcatid}}">
                  <button type="submit" class="btn btn-info btn-block rounded-0">Submit</button>
                </form>  
                </div>
              </div>
             <?php
              foreach($data['actualData'] as $datas){
              $id = $datas['id'];?>
              <div class="col-12 col-sm-6 col-md-3">
                <div class="col_selector mb-4">
                  <div class="col_image img_hover">
                    <img src="<?= $datas['images'] ?>" alt="" class="img-fluid w-100 data_img">
                    <p class="fz26 m-0 hover_icon">
                      <a href="javascript:void(0);" class="mr-3 view_img_lg_trigger" data-toggle="modal" data-target="#img_view"><i class="fa fa-eye"></i></a>
                      <a href="javascript:void(0);" onclick="myFunction('<?=$id?>')"><i class="fa fa-trash"></i></a>
                      <a href="{{url('deleteimage',['id'=>$id])}}" id="<?=$id?>" class="invisible"><i class="fa fa-trash"></i></a>
                    </p>
                  </div>
                </div>
                <!-- // image view modal start here -->
                <div class="modal fade" id="img_view" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-signup" role="document">
                    <div class="modal-content">
                      <div class="card card-signup card-plain m-0">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <i class="material-icons">clear</i>
                          </button>
                        </div>
                        <div>
                            <img src="" class="img-fluid" alt="" id="view_img_lg">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                 <!-- // image view modal end here -->
              </div>
              <?php }?>
            </div>
          </div>
      </div>
    
@include('include.footer')

<script>
   function myFunction(id)
   {
     if(confirm("Want to delete?"))
     {
      document.getElementById(id).click();
     }  
   }

//error fadeout
$('.hide').fadeOut(5000);

</script>     