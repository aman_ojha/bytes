@include('include.header')
<?php $categoryid = $data['categoryid'];?>
<script type="text/javascript" src="{{ asset('public/assets/js/subcategory.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if (\Session::has('success'))
<script>
swal ( "Success" ,  "Data has been saved" ,  "success" );
</script>
@endif
@if (\Session::has('update'))
<script>
swal ( "Update" ,  "Data update successfully" ,  "update" );
</script>
@endif
  <div ng-app="SubCategoryListing">
    <div ng-controller="SubCategoryController">
      <div class="content page_data">
        <div class="mb-5 clearfix">
           @if($errors->has('filename'))
              <div class="row hide">
                <div class="col-lg-12">
                  <div class="alert alert-danger">
                      <span>{{ $errors->first('filename') }}</span>
                  </div>
                </div>
              </div>
          @endif
          <p class="pull-left mb-0 fz35 pt-3">Sub Category</p>
          <button class="btn btn-info text-uppercase pull-right" data-toggle="modal" data-target="#signupModal">Add SubCategory</button>
            <!-- add modal start here -->
              <div class="modal fade" id="signupModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-signup" role="document">
                  <div class="modal-content">
                    <div class="card card-signup card-plain m-0">
                      <div class="modal-header">
                        <h5 class="modal-title card-title">Add SubCategory</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="material-icons">clear</i>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form action="{{url('addsubcategory')}}" class="w-100 pl-4 pr-4" method="post" enctype="multipart/form-data">
                          <div class="row">
                            <div class="col-12">
                              <label for="customFile" class="mb-0">
                                  <img src="#" class="img-fluid rounded" alt="" id="Profile2" width="50%" height="50%">
                                  <span class="align-bottom ml-3">Upload Photo</span>
                              </label>
                              <input type="file" name="filename" class="custom-file-input d-none" id="customFile" onchange="readURL(this,'Profile2')">
                            </div>                                                        
                            <div class="form-group col-12 mt-4">                              
                              <input type="text" class="form-control" id="inputEmail4" placeholder="Enter SubCategory Name" name="subcatname" required="" style="text-transform: capitalize;">
                            </div>
                           <!--  <div class="col-12">
                              <div class="col_selector mt-3">
                                <label class="mr-5">                                    
                                  <span>Category Price : </span>
                                </label>
                                <div class="form-check form-check-radio form-check-inline mr-5">
                                  <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Free
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                                <div class="form-check form-check-radio form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Paid
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                              </div>
                            </div> -->
                            <!-- <div class="form-group col-12">                              
                              <input type="email" class="form-control" id="inputEmail4" placeholder="Enter Price">
                            </div> -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="catid" value="{{$categoryid}}">
                            <div class="col-12">
                              <div class="col_selector text-right">
                                <button type="submit" class="btn btn-info mr-3">Save</button>
                                <button  class="btn btn-info" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- add modal end here -->
        </div>
          <div class="custom_table">
            <div class="table-responsive">
              <table class="table" id="myTable">
                <thead>
                    <tr>
                        <th class="text-center">S.No.</th>
                        <th class="text-center">Photo</th>
                        <th>SubCategory Name</th>
                        <th class="text-center">Edit</th>
                        <th class="text-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $i=1; 
                foreach($data['actualData'] as $datas){
                   $id = $datas['id'];
                    $a = Crypt::encrypt($id);?>
                    <tr>
                        <td class="text-center"><?= $i;?></td>
                        <td class="text-center">
                         <div class="uploadedImgWrapper">  
                          <img src="<?= $datas['icon_img'] ?>" alt="" class="rounded-circle" onclick="openviewImg()">
                         </div> 
                        </td>
                        <td><a href="{{url('imagesview',['id'=>$a])}}"><?= $datas['cat_name'] ?></a></td>
                       <td class="text-center"><a ng-click = "fetchdata('<?= $datas['id']?>','<?= $datas['icon_img']?>','<?= $datas['cat_name']?>','<?= csrf_token() ?>')" data-toggle="modal" data-target="#edit_category"><i class="fa fa-pencil fz26 base_color" ></i></a></td>
                       <td class="td-actions text-center">
                        <div id="div<?=$datas['id']?>">
                            <button type="button" ng-click = "updatestatus('<?= $datas['active_status']?>','<?= $datas['id']?>')" class="btn btn-info pt-2 pb-2 pl-3 pr-3 statusbtn"><?=  $datas['active_status']? 'Disable':  'Enable'?></button>       
                        </div>    
                        </td>
                    </tr>
                <?php $i++; } ?>    
                </tbody>
            </table>
            </div>

          </div>
      </div>


      <!-- add edit_category modal start here -->
        <div class="modal fade" id="edit_category" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-signup" role="document">
            <div class="modal-content">
              <div class="card card-signup card-plain m-0">
                <div class="modal-header">
                  <h5 class="modal-title card-title">Edit Category</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <i class="material-icons">clear</i>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <form action="{{url('updatesubcategory')}}" class="w-100 pl-4 pr-4"  method="post" enctype="multipart/form-data">
                      <div class="col-12">
                        <label for="customFile2" class="mb-0">
                            <img src="@{{image}}" class="img-fluid rounded" alt="" id="Profile" width="50%" height="50%">
                            <span class="align-bottom ml-3">Upload Photo</span>
                        </label>
                         <input type="file" name="filename" class="custom-file-input d-none"  id="customFile2" onchange="readURL(this,'Profile')">
                      </div>                                                        
                      <div class="form-group col-12 mt-4">                              
                        <input type="text" class="form-control" id="inputEmail4" ng-model="cat" placeholder="Enter SubCategory Name" name="category" required="" style="text-transform: capitalize;">
                      </div>
                     <!--  <div class="col-12">
                        <div class="col_selector mt-3">
                          <label class="mr-5">                                    
                            <span>Category Price : </span>
                          </label>
                          <div class="form-check form-check-radio form-check-inline mr-5">
                            <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Free
                              <span class="circle">
                                  <span class="check"></span>
                              </span>
                            </label>
                          </div>
                          <div class="form-check form-check-radio form-check-inline">
                            <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Paid
                              <span class="circle">
                                  <span class="check"></span>
                              </span>
                            </label>
                          </div>
                        </div>
                      </div> -->
                     <!--  <div class="form-group col-12">                              
                        <input type="email" class="form-control" id="inputEmail4" placeholder="Enter Price">
                      </div> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="userid" ng-modal="userid" value=@{{userid}}>
                      <div class="col-12">
                        <div class="col_selector text-right">
                          <button type="submit" class="btn btn-info mr-3">Save</button>
                          <button  class="btn btn-info" data-dismiss="modal">Cancel</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <!-- add modal end here -->
    </div>
  </div>  
@include('include.footer')

<!-- Script Strat  -->  
<script>

//error fadeout
  $('.hide').fadeOut(5000);

//Datatable use
  $(document).ready( function () {
    $('#myTable').DataTable();
  });

//show image on select
function readURL(input,input2) 
{
  var id = input2;
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+id)
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
$(".statusbtn").click(function () {
    $(this).text(function(i,v){
       return v == 'Enable' ? 'Disable' : 'Enable'
    })
  });  

</script> 
<!-- Script End -->