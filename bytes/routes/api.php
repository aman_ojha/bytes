<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix'=>'user'],function(){
Route::post('register', 'Api\V1\UserController@register');
Route::post('login', 'Api\V1\UserController@login');
Route::post('social','Api\V1\UserController@social');
Route::post('sendmail','Api\V1\UserController@sendmail');
});

Route::group(['middleware' => 'jwt.auth'], function () {
Route::get('user','Api\V1\UserController@getAuthUser');
Route::get('getallcategory','Api\V1\CategoryController@getallcategory');
Route::post('getallsubcategory','Api\V1\CategoryController@getallsubcategory');
Route::post('getallimages','Api\V1\CategoryController@getallimages');
Route::post('editprofile','Api\V1\UserController@editprofile');
Route::post('changepass','Api\V1\UserController@changepass');
Route::post('addfavourite','Api\V1\CategoryController@addfavourite');
Route::post('favouritelist','Api\V1\CategoryController@favouritelist');
Route::post('removefavourite','Api\V1\CategoryController@removefavourite');
Route::post('favouriteimage','Api\V1\CategoryController@favouriteimage');
Route::post('favouritecatlist','Api\V1\CategoryController@favouritecatlist');
Route::post('contactus','Api\V1\UserController@contactus');
Route::get('professionalist','Api\V1\UserController@professionalist');
Route::post('savetoken','Api\V1\UserController@savetoken');
});
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
