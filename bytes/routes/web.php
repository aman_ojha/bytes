<?php
//Auth::routes();
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// APP API
Route::get('/','Auth\AdminController@adminlogin');
Route::post('admin-login', ['as'=>'admin-login','uses'=>'Auth\AdminController@checkadmin']);
Route::get('/logout', 'Auth\LogoutController@logout');
Route::group(['middleware'=>'auth:admin'],function(){
	//Route::get('/category_images', 'Auth\CategoryImagesController@categoryview');
	Route::get('/listing', 'Auth\ListingController@listview');
	Route::get('/subcategory', 'Auth\SubCategoryController@subcategoryview');
	Route::get('/categorymanagment', 'Auth\CategoryManagementController@categorymanagementview');
	Route::post('/addcategory','Auth\CategoryManagementController@addcategory');
	Route::get('/categorylist','Auth\CategoryManagementController@categorylist');
	Route::get('/updateshareable','Auth\UpdateController@updateshareable');
	Route::post('/updatestatus','Auth\UpdateController@updatestatus');
	Route::get('/updatesubcatstatus', 'Auth\UpdateController@updatesubcatstatus');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::post('/updatecategory', 'Auth\UpdateController@updatecategory');
	Route::get('/subcategoryview/{id}', 'Auth\CategoryManagementController@subcategoryview');
	Route::post('/addsubcategory', 'Auth\CategoryManagementController@addsubcategory');
	Route::post('/updatesubcategory', 'Auth\UpdateController@upadtesubcategory');
	Route::get('/imagesview/{id}', 'Auth\CategoryManagementController@imagesview');
	Route::post('/addimages', 'Auth\CategoryImagesController@addimages');
	Route::get('/updateuserstatus', 'Auth\ListingController@updateuserstatus');
	Route::get('/deleteimage/{id}', 'Auth\CategoryImagesController@deleteimage');
	Auth::routes();
});
Route::get('/success',function(){
     return view('success');  
});
Route::get('/forgetpass/{id}','Auth\ForgotPasswordController@forget');
Route::get('/adminforget/{id}','Auth\ForgotPasswordController@adminforget');
Route::get('sendmail','Auth\ForgotPasswordController@sendmail');
Route::post('/updatepass', 'Auth\UpdateController@updatepass');
Route::post('/updateadminpass', 'Auth\UpdateController@updateadminpass');
Route::get('/success', function () {
    return view('success');
});
Route::get('/adminpass', function () {
    return view('adminpass');
});
Route::get('/linksend', function () {
    return view('linksend');
});
Route::get('/error', function () {
    return view('error');
});
Route::get('/contact', function () {
    return view('emails.contact');
});




//WEB API

