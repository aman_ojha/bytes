<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId','catId','subId','imageId','fav_status','timeStamp'
    ];

}
