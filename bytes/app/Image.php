<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'images','subcat_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     * 
     **/
}
