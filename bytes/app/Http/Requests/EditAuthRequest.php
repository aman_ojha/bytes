<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'id' => 'required|integer',
            'name' => 'required|string',
            'email' => 'required',
            'phone_no' => 'required|string',
            'gender' => 'required|string',
            'country' => 'required|string',
            'professionalDomain' => 'required|string',
            'country_code' => 'required|string',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048'
        ];
    }
}
