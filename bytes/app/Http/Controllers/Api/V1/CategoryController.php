<?php



namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\FavouriteAuthRequest;

use App\Http\Controllers\Controller;

use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Illuminate\Http\Response;

use App\Favourite;

use App\Category;

use App\Image;

use JWTAuth;



class CategoryController extends Controller

{

    public function getallcategory()
    {

         $category = new Category();
         
         $data = Category::where('parent_id',0)->where('active_status',1)->get();

         return response()->json([

                        'success' => true,

                        'data' => $data

                        ], 200);

    }

    public function getallsubcategory(Request $request)
    {

    	$category = new Category();

    	$data = Category::Where('parent_id',$request->catid)->Where('active_status',1)->get();

    	return response()->json([

    	                'success' => true,

                        'data' => $data

                        ], 200);
      


    }

    public function getallimages(Request $request)
    {
        $images = new Image();
        $favourite = new Favourite();
        $subId = $request->subcatid;
        $userId = $request->userId;
        $data = Image::SELECT(['images.id as image_id','images.images','images.subcat_id','categories.parent_id as cat_id'])->join('categories','categories.id','=','images.subcat_id')->Where('images.subcat_id',$subId)->get();

        $count = count($data);
        if($count>0) 
        { 

            foreach($data as $datas)
            {

               $imageId = $datas->image_id;
               
               $wheredata = ['userId'=>$userId,'subId'=>$subId,'imageId'=>$imageId];

               $favstatus = Favourite::SELECT(['fav_status'])->Where($wheredata)->first();
              
               if($favstatus)
               {
                   $datas['fav_status'] = $favstatus->fav_status;
               }
               else
               {
                   $datas['fav_status'] = 0;
               } 
               
               $alldata[] = $datas;    
            }
            
            

            return response()->json([

                            'success' => true,

                            'data' => $alldata

                            ], Response::HTTP_OK);
        }
        else
        {
          return response()->json([

                            'success' => true,

                            'data' => []

                            ], Response::HTTP_OK);
        }    
    }

    public function addfavourite(FavouriteAuthRequest $request)
    {

       $favourite = new Favourite();

       $images = new Image();

       $favourite->userId = $request->userId;

       $favourite->catId = $request->catId;

       $favourite->subId = $request->subId;

       $favourite->imageId = $request->imageId;

       $favourite->fav_status = 1;

       $favourite->timeStamp = round(microtime(true) * 1000);
       
       $count = Favourite::Where('subId',$favourite->subId)->count();
       
       if($count==5)
       {
          
          return response()->json([

                        'success' => false,

                        'message' => 'Can not add more than five favourite image', 

                        'data' => $favourite

                        ], Response::HTTP_FOUND);
       }
       else
       {	

	          $insert = $favourite->save();

	            if($insert)
	             {
	                return response()->json([

	                            'success' => true,

	                            'message' => 'This item added in your favourite list', 

	                            'data' => $favourite

	                            ], Response::HTTP_CREATED);
	             }
	            else
	            {
	                return response()->json([

	                            'success' => false,

	                            'message' => 'Something went wrong', 

	                            'data' => $favourite

	                            ], Response::HTTP_BAD_REQUEST);
	            } 
	       }
	   }       

    public function favouritecatlist(Request $request)
    {
    	$list = DB::table('favourites')
    	            ->join('categories','categories.id', '=', 'favourites.catId')
    	            ->select('categories.id','categories.cat_name','categories.icon_img')
    	            ->distinct('categories.id')
    	            ->where('favourites.userId',$request->userId)
                    ->where('favourites.status',1)
                    ->get();        
        if($list)
        {
            return response()->json([

                            'success' => true,

                            'message' => 'All favourite list', 

                            'data' => $list

                            ], Response::HTTP_OK);
        }
        else
        {
            return response()->json([

                            'success' => true,

                            'message' => 'No list', 

                            'data' => []

                            ], Response::HTTP_OK);
        }                                  
    }

    public function favouritelist(Request $request)
    {

       $list =  DB::table('favourites')
                  ->select('categories.id','categories.cat_name','categories.icon_img','favourites.timeStamp')
                  ->join('categories','categories.id', '=', 'favourites.subId')
                  ->where('favourites.userId',$request->userId)
                  ->where('favourites.catId',$request->catId)
                  ->where('favourites.status',1)
                  ->groupBy('categories.id')
                  ->get();
      
        if($list)
        {
            return response()->json([

                            'success' => true,

                            'message' => 'All favourite list', 

                            'data' => $list

                            ], Response::HTTP_OK);
        }
        else
        {
            return response()->json([

                            'success' => true,

                            'message' => 'No list', 

                            'data' => []

                            ], Response::HTTP_OK);
        }                                 
    }

    public function favouriteimage(Request $request)
    {
       $images = DB::table('favourites')
                     ->join('images','images.id', '=','favourites.imageId')
                     ->select('images.images')
                     ->where('favourites.subId',$request->subId)
                     ->get();
       if($images)
       {
            return response()->json([

                            'success' => true,

                            'message' => 'All favourite images found', 

                            'data' => $images

                            ], Response::HTTP_OK);
       }
       else
       {
           return response()->json([

                            'success' => true,

                            'message' => 'No list', 

                            'data' => []

                            ], Response::HTTP_OK);

       }              
    }

    public function removefavourite(Request $request)
    {
        $favourite = new Favourite();
         
        $userId = $request->userId;
        $subId = $request->subId;
        $imageId = $request->imageId;

        $wheredata = ['userId'=>$userId,'subId'=>$subId,'imageId'=>$imageId];

        $delete = Favourite::where($wheredata)->delete();

        if($delete)
        {
            return response()->json([

                            'success' => true,

                            'message' => 'Remove from favourite successfully', 

                            'data' => []

                            ], 200);
        }
        else
        {
            return response()->json([

                            'success' => false,

                            'message' => 'No favourite list found', 

                            'data' => []

                            ], 202);
        }
    }




}