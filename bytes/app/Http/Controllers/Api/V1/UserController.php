<?php

namespace App\Http\Controllers\Api\V1;
use App\Http\Requests\RegisterAuthRequest;
use App\Http\Requests\EditAuthRequest;
use App\Http\Requests\FacebookAuthRequest;
use App\Http\Requests\TokenAuthRequest;
use App\User;
use App\Professionaldomain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Mail;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Token;

class UserController extends Controller
{
    public $loginAfterSignUp = true;



//Register User
			    public function register(RegisterAuthRequest $request)
			    {
                        $user = new User();   
	                    $user->name = $request->name;
	                    $user->email = $request->email;
	                    $user->country = $request->country;
	                    $user->phone_no = $request->phone_no;
                        $user->country_code = $request->country_code;
	                    $user->password = bcrypt($request->password);
	                    $user->save();
	             
	                    if ($this->loginAfterSignUp) {
	                        return $this->login($request);
	                    }
	             
	                    return response()->json([
	                        'success' => true,
	                        'data' => $user
	                    ], 200);
			    }


//Login User 
                public function login(Request $request)
                {
                    
                    $input = $request->only('email', 'password');
                    $jwt_token = null;
                    $myTTL = 10080; //minutes
                    JWTAuth::factory()->setTTL($myTTL);
                    if (!$jwt_token = JWTAuth::attempt($input)) {
                        return response()->json([
                            'success' => false,
                            'message' => 'Invalid Email or Password',
                        ], 401);
                    }
                  $user =  $this->getData($request);
                  $user->token = $jwt_token;
                    return response()->json([
                        'success' => true,
                        'data' => $user
                    ]);
                }



//Facebook Login
                public function social(FacebookAuthRequest $request)
                {
                      
                      $input = $request->only('social_id');
                      $input['password'] = "12345678";
                      $jwt_token = null;
                      $user = new User(); 
                      $user->name = $request->name;
                      $user->email = $request->email;
                      $user->social_id = $request->social_id;
                      $user->password = bcrypt('12345678');
                      if($request->has('image'))
                     { 
                       $extns = $request->image->getClientOriginalExtension();
                       $imageName = rand().time().'.'.$request->image->getClientOriginalExtension();
                       $request->image->move(public_path('user-images/'), $imageName);
                       $user->image = url('public/user-images/').'/'.$imageName;
                     }
                      $finduser =  DB::table('users')->where('social_id', $user->social_id)->first();
                      if($finduser)
                      {
                        if (!$jwt_token = JWTAuth::attempt($input)) 
                        {
                            return response()->json([
                                'success' => false,
                                'message' => 'Invalid Email or Password',
                            ], 401);
                        }
                        $finduser->token = $jwt_token;
                        return response()->json([
                        'success' => true,
                        'data' => $finduser
                        ], 200);
                      }
                      else
                      {
                        $user->save();
                        $data = DB::table('users')->where('id', $user->id)->first();
                        if (!$jwt_token = JWTAuth::attempt($input)) 
                        {
                            return response()->json([
                                'success' => false,
                                'message' => 'Invalid Email or Password',
                            ], 401);
                        }
                        $data->token = $jwt_token;
                        return response()->json([
                        'success' => true,
                        'data' => $data
                        ], 200);
                      }
                      
                }

//edit profile
                public function editprofile(EditAuthRequest $request)
                {
                        $user = new User();
                        $user->id = $request->id;
                        $user = User::find($user->id);   
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->phone_no = $request->phone_no;
                        $user->gender = $request->gender;
                        $user->country = $request->country;
                        $user->country_code = $request->country_code;
                        $user->professionalDomain = $request->professionalDomain;
                        $check = DB::table('users')->where('email',$user->email)->where('id','!=',$user->id)->first();
                        if($check)
                        {
                           return response()->json([
                             'success' => false,
                             'message' => 'Email already exist',
                            ], 201);
                        }
                        if($request->has('image'))
                        { 
                           $extns = $request->image->getClientOriginalExtension();
                           $imageName = rand().time().'.'.$request->image->getClientOriginalExtension();
                           $request->image->move(public_path('user-images/'), $imageName);
                           $user->image = url('public/user-images/').'/'.$imageName;
                        }
                        $user->save();
                        return response()->json([
                            'success' => true,
                            'data' => $user
                        ], 200);
                }
//change password

                public function changepass(Request $request)
                {

                      $user = new User();
                      $jwt_token = null;
                      $user->id = $request->id;
                      $oldpass = $request->oldpass;
                      $user->password = $request->newpass;
                      $currentpass = User::where('id',$user->id)->first();
                      $password = $currentpass->password;

                      if(Hash::check($oldpass,$password))
                      {
                      
                        $update = User::where('id',$user->id)->update(['password' => bcrypt($user->password)]);
                        if($update)
                        {
	                        $data = User::where('id',$user->id)->select('email')->first();
                          $input['email'] = $data->email;
                          $input['password'] = $user->password;
                          if (!$jwt_token = JWTAuth::attempt($input)) 
                          {
                              return response()->json([
                                  'success' => false,
                                  'message' => 'Invalid Email or Password',
                              ], 401);
                          }
                          $user->token = $jwt_token;
	                          return response()->json([
	                            'success' => true,
	                            'message' =>'password changed successfully',
	                            'data' => $user
	                        ], 200);
                        }
                      }
                      else
                      {
	                          return response()->json([
	                            'success' => false,
	                            'message' =>'something went wrong',
	                            'data' => $user
	                        ], 200);
                      }


                }
//sendmail
                public function sendmail(request $request)
                {
                    $email = $request->email;
                    $data = DB::table('users')->where('email',$email)->first();
                    if($data)
                    {
                      $idd = DB::table('users')->where('email',$email)->select('id')->pluck('id')->first();
                      $id = Crypt::encrypt($idd);
                      $user = array('email'=>$email,'name'=>'Rahul');
                      Mail::send('emails.welcome', ['user' => "http://instabytes.in/instabytes/forgetpass/$id", 'message' => ""], function ($message) use ($user)
                         {
                            $message->from('info@instabytes.in', 'Instabytes.in');
                            $message->to($user['email'], $user['name'])->subject('Welcome to Instabytes');
                        }); 

                      return response()->json(['msg'=>"Email has sent",'status'=>200]); 
                    }
                    else
                    {
                      return response()->json(['msg'=>"Email does not exist",'status'=>202]);
                    }
                    
                }

//save token
                public function savetoken(TokenAuthRequest $request)
                {
                      $token = new Token();
                      $token->user_id = $request->user_id;
                      $token->device_id = $request->device_id;
                      $token->token_id = $request->token_id;
                      $save = $token->save();
                      if($save)
                      {
                           return response()->json([
                              'success' => true,
                              'message' =>'token save successfully',
                              'data' => $token
                          ], 200);
                      }
                       else
                      {
                            return response()->json([
                              'success' => false,
                              'message' =>'something went wrong',
                              'data' => $token
                          ], 202);
                      }

                }

//contact us email

                public function contactus(request $request)
                {
                   $name = (string)$request->name;
                   $email = (string)$request->email;
                   $phone = (string)$request->phone;
                   $subject = (string)$request->subject;
                   $message = (string)$request->message;
                   $email =  DB::table('admins')->where('id',1)->select('email')->pluck('email')->first();
                   $user = array('email'=>$email,'name'=>'Rahul');
                   if($email)
                   { 

                      Mail::send('emails.contact', ['name' => $name,'email'=>$email, 'phone' => $phone, 'subject' => $subject ,'sms' => $message], function ($message) use ($user)
                         {
                            $message->from('info@instabytes.in', 'Instabytes.in');
                            $message->to($user['email'], $user['name'])->subject('Welcome to Instabytes');
                        }); 
                      return response()->json(['msg'=>"Email has sent",'status'=>200]); 
                   }
                    else
                    {
                      return response()->json(['msg'=>"Email does not exist",'status'=>202]);
                    }
                    
                }
//ProfessionalDomain
 
                public function professionalist()
                {
                     $user = new Professionaldomain();

                     $data = Professionaldomain::get();

                    if($data)
                    {

                        return response()->json([

                                        'success' => true,

                                        'data' => $data

                                        ], Response::HTTP_OK);
                    }
                    else
                    {
                        return response()->json([

                                        'success' => true,

                                        'data' => []

                                        ], Response::HTTP_OK);
                    }
                }                                                                                  
                            
             
    // public function logout(Request $request)
    // {
    //     $this->validate($request, [
    //         'token' => 'required'
    //     ]);
 
    //     try {
    //         JWTAuth::invalidate($request->token);
 
    //         return response()->json([
    //             'success' => true,
    //             'message' => 'User logged out successfully'
    //         ]);
    //     } catch (JWTException $exception) {
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Sorry, the user cannot be logged out'
    //         ], 500);
    //     }
    // }
 
    public function getAuthUser(Request $request)
    {
        $token = $request->bearerToken();


        
         
        $user = JWTAuth::authenticate($token);
        //dd($user);
        return response()->json(['user' => $user]);
    }
    public function getData(Request $request)
    {
        $email = $request->email;
        $password = bcrypt($request->password);
        $result = DB::table('users')->where('email', $email)->first();
        return $result;

    }
    
}
