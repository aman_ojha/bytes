<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;

class ListingController extends Controller
{
     public function listview(Request $request)
    {
       $result = User::get(['id','name','email','phone_no','country','status']);
       $data = $result->toArray();
       return view('listing',compact('data'));
    }

     public function updateuserstatus(Request $request)
    {

        $affected = DB::update("UPDATE `users` SET `status` = NOT (status) where id='$request->id'");
        return response()->json([
                          'success' => true,
                      ]);
    }
}
