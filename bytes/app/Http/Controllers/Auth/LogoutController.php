<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
class LogoutController extends Controller
{

    public function logout (Request $request)
    {
    	Auth::guard('admin')->logout();
        return redirect('/');
    }

}
