<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Category;
use App\Image;
use DB;
use Mail;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Support\Facades\Crypt;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }

    public function forget($id)
    {
       $idd = Crypt::decrypt($id); 
       $data['id'] = $idd; 
       return view('newpass',compact('data'));
    }

    public function adminforget($id)
    {
       $idd = Crypt::decrypt($id); 
       $data['id'] = $idd; 
       return view('adminpass',compact('data'));
    }

    public function sendmail()
    {
        $data = DB::table('admins')->where('id',1)->select('email')->pluck('email')->first();
        if($data)
        {
          $idd = 1;  
          $id = Crypt::encrypt($idd);
          $user = array('email'=>$data);
        Mail::send('emails.welcome', ['user' => "http://instabytes.in/instabytes/adminforget/$id", 'message' => ""], function ($message) use ($user)
             {
                $message->from('info@instabytes.in', 'Instabytes.in');
                $message->to($user['email'])->subject('Welcome to Instabytes');
            }); 

          return Redirect('/linksend');
        }
        else
        {
          return Redirect('/error');
        }
        
    }                
}
