<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Image;
use Validator;
class CategoryImagesController extends Controller
{
  
  
    // public function categoryview()
    // {
    //    return view('category_images');
    // }

    public function addimages(Request $request)
    {

      $this->validate($request, [
                'filename' => 'required',
                'filename.*' => 'image|mimes:jpeg,png,jpg|max:2048',
        ],[
        'filename.required' => 'Please upload an image',
        'filename.*.mimes' => 'Only jpeg,png and bmp images are allowed',
        'filename.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
        ]);
   
            $images = new Image();
        	if($request->has('filename'))
            {
               foreach($request->file('filename') as $image)
               {	
    	      	   $extns = $image->getClientOriginalExtension();
    	      	   $imageName = rand().time().'.'.$image->getClientOriginalExtension();
    	      	   $image->move(public_path('admin-images/'), $imageName); 
    	           $images_url = url('public/admin-images/').'/'.$imageName;
    	           $images->images = $images_url;
    	           $images->subcat_id = $request->subid;
    	           $images->save();
    	           $images = new Image();                                                             //recreate object for multiple upload;
    	       }    
            }
        	 return  Redirect::back()->withInput()->with('success', 'Successfully');;     
    }

    public function deleteimage($id)
    {
         $images = new Image();

         $delete = Image::Where('id',$id)->delete();

         return  Redirect::back()->withInput()->with('deleted', 'Successfully');;
    }
    
}
