<?php

namespace App\Http\Controllers\Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Admin;
use Auth;
use DB;
use Session;

class AdminController extends Controller
{

    use AuthenticatesUsers;

    protected $guard = 'admin';

    public function __construct()
    {
      
      $this->middleware('guest:admin');
    }


    public function adminlogin()
    {

        return view('adminlogin');
    }

    public function checkadmin(Request $request)
    {

    	$validator = Validator::make(collect($request)->toArray(), [
            'userName' => 'required|max:75',
            'password'=>'required|min:6|max:25'
       ]);
       if ($validator-> fails())
       {
          return back()->withErrors(['userName' => 'The username or password you enterd is incorrect.']);
           
       }
       else
       {
    
            $username = $request->userName;
            $password = $request->password;
            if (auth()->guard('admin')->attempt(['userName' => $username, 'password' => $password]))
            {
                $authData = auth()->guard('admin')->user();
                $user = $authData->userName;
                 Session::put('authData',  $user);
                return redirect('/categorymanagment');
            }
	        else
	        {
	        	 return back()->withErrors(['userName' => 'The username or password you enterd is incorrect.']);
	        }

 


        }
    }

    public function addsubcategory(Request $request)
    {
       dd($request->all());
       $category = new Category();
       $extns = $request->filename->getClientOriginalExtension();
       $imageName = rand().time().'.'.$request->filename->getClientOriginalExtension();
       $request->filename->move(public_path('admin-images/'), $imageName);
       $category->cat_name = $request->category;
       $category->icon_img = url('public/admin-images/').'/'.$imageName;
       $category->parent_id = $request->categoryid;
       $category->save();
       return  Redirect::back()->withInput()->with('success', 'Successfully');

    } 

   
}
