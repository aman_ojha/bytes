<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Image;
use DB;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Support\Facades\Crypt;


class CategoryManagementController extends Controller
{


  //CategoryManagementView 
    public function categorymanagementview(Request $request)
    {
       $result = Category::Where('parent_id',0)->get();
       $data = $result->toArray();
       return view('category_managment',compact('data'));
    }


  //AddCategory
    public function addcategory(Request $request)
    {

       $validator = Validator::make(collect($request)->toArray(), [
           'filename' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($validator->fails())
        {

           return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {   

         $category = new Category();
         if($request->has('filename'))
         { 
      	   $extns = $request->filename->getClientOriginalExtension();
      	   $imageName = rand().time().'.'.$request->filename->getClientOriginalExtension();
      	   $request->filename->move(public_path('admin-images/'), $imageName);
           $category->icon_img = url('public/admin-images/').'/'.$imageName;
         }
         $category->cat_name = ucfirst($request->category); 
         $category->remember_token = $request->_token;
         if($request->paymode == "paid"){
         		 $category->price = $request->price;
         }else{
         		$category->price = "free";
         }
         $category->save();
         return  Redirect::back()->withInput()->with('success', 'Successfully');

        } 
    }
  

  //SubcategoryView
    public function subcategoryview($id)
    {
      $idd = Crypt::decrypt($id);
      $result = Category::Where('parent_id', $idd)->get();
      $data['actualData'] = $result->toArray();
      $data['categoryid'] = $idd;
      return view('sub_category',compact('data'));
    }  
  

  //Addsubcategory
    public function addsubcategory(Request $request)
    {
        $validator = Validator::make(collect($request)->toArray(), [
           'filename' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($validator->fails())
        {
           return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
         $category = new Category();
         if($request->has('filename'))
         { 
           $extns = $request->filename->getClientOriginalExtension();
           $imageName = rand().time().'.'.$request->filename->getClientOriginalExtension();
           $request->filename->move(public_path('admin-images/'), $imageName);
           $category->icon_img = url('public/admin-images/').'/'.$imageName;
         }
         $category->cat_name = ucfirst($request->subcatname);  
         $category->parent_id = $request->catid;
         $category->remember_token = $request->_token;
         $category->price = "";
         $category->save();
         return  Redirect::back()->withInput()->with('success', 'Successfully');
        } 

    }  
   
  //Imagesview
    public function imagesview($id)
    {
      $idd = Crypt::decrypt($id);
      $result = Image::Where('subcat_id',$idd)->get();
      $data['actualData'] = $result->toArray();
      $data['subcatid'] = $idd;
      return view('category_images',compact('data'));
    }

    


}
