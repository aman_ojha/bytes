<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
//use APP\User;
use App\User;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Crypt;


class UpdateController extends Controller
{
     public function updateshareable(Request $request)
    {
        
        $affected = DB::update("UPDATE `categories` SET `shareable` = NOT (shareable) where id='$request->id'");
        return response()->json([
                          'success' => true,
                      ]);
    }

     public function updatestatus(Request $request)
    {
        
       $affected = DB::update("UPDATE `categories` SET `active_status` = NOT (active_status) where id='$request->id'");
        return response()->json([
                          'success' => true,
                      ]);
    }

    public function updatecategory(Request $request)
    {

        $validator = Validator::make(collect($request)->toArray(), [
             'filename' => 'image|mimes:jpeg,png,jpg|max:2048',
          ]);

          if ($validator->fails())
          {
             return Redirect::back()->withInput()->withErrors($validator);
          }
          else
          {
           $category = new Category();
           $data = array(); 
           if($request->has('filename'))
           {
             $extns = $request->filename->getClientOriginalExtension();
             $imageName = rand().time().'.'.$request->filename->getClientOriginalExtension();
             $request->filename->move(public_path('admin-images/'), $imageName);
             $category->icon_img = url('public/admin-images/').'/'.$imageName;
             $data['icon_img'] = $category->icon_img;
           } 
           $category->id = $request->userid;
           $category->cat_name = ucfirst($request->category);
           $category->remember_token = $request->_token;
           if($request->paymode == "paid"){
                 $category->price = $request->price;
           }else{
                $category->price = "free";
           }
           $data['cat_name'] = $category->cat_name;
           $data['price'] = $category->price;
           $data['remember_token'] = $category->remember_token;
           $update = Category::where('id',$category->id)->update($data);
           return  Redirect::back()->withInput()->with('update', 'Successfully');
          } 
    }

    public function updatesubcatstatus(Request $request)
    {
        
        $affected = DB::update("UPDATE `categories` SET `active_status` = NOT (active_status) where id='$request->id'");
        return response()->json([
                          'success' => true,
                      ]);
      
    }

    public function upadtesubcategory(Request $request)
    {
         $validator = Validator::make(collect($request)->toArray(), [
             'filename' => 'required|image|mimes:jpeg,png,jpg|max:2048',
          ]);

          if ($validator->fails())
          {
             return Redirect::back()->withInput()->withErrors($validator);
          }
          else
          {
            $category = new Category();
            $data = array(); 
            if($request->has('filename'))
               {
                 $extns = $request->filename->getClientOriginalExtension();
                 $imageName = rand().time().'.'.$request->filename->getClientOriginalExtension();
                 $request->filename->move(public_path('admin-images/'), $imageName);
                 $category->icon_img = url('public/admin-images/').'/'.$imageName;
                 $data['icon_img'] = $category->icon_img;
               } 
               $category->id = $request->userid;
               $category->cat_name = ucfirst($request->category);
               $category->remember_token = $request->_token;
               $data['cat_name'] = $category->cat_name;
               $data['remember_token'] = $category->remember_token;
               $update = Category::where('id',$category->id)->update($data);
               return  Redirect::back()->withInput()->with('update', 'Successfully');
          }     
    }

    public function updatepass(Request $request)
    {
        $password = bcrypt($request->password);
        $update = DB::update("UPDATE users SET password = '$password' WHERE id =$request->userid");
        if($update)
        {
         return  redirect('/success');
        }
        else
        {
          return  Redirect::back()->withInput()->with('otherupdate', 'Successfully');
        }
    }

    public function updateadminpass(Request $request)
    {
      $password = bcrypt($request->password);
      $update = DB::update("UPDATE admins SET password = '$password' WHERE id = 1");
      if($update)
      {
         return  redirect('/success');
      }
      else
      {
        return  redirect('/error');
      }
    }
   
}
